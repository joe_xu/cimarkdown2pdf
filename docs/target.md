# Flask-RESTPlusでAPI管理
*参照：(https://qiita.com/Aruneko/items/2adbf12bb5bace32e002.md)*
#### Python version 3.6.7

## 大枠
### 基本のファイルの構造

```
rest_api/
├── api/
│   ├── router.py
│   └── __init__.py
├── model(今回使っていない)/
│   └── __init__.py
├── sharelib/
│   └── dbconnect.py
├── app.py
└── settings.py
```

### APIエンドポイントの準備
```python3:api/__init__.py
from flask_restplus import Api

# API情報を指定して初期化
api = Api(
    title='Test API',
    version='1.0',
    description='Swaggerを統合したREST APIのサンプル'
)
```

### アプリの設定
```python3:settings.py
from os import environ

# デバッグモードを有効化
DEBUG = True

# Swaggerのデフォルト表示形式をListにする
SWAGGER_UI_DOC_EXPANSION = 'list'
# Validationの有効化
RESTPLUS_VALIDATE = True

# SQL接続情報(今回使っていない)
# コンテナ側に環境変数として渡すためこの形式で受け取る
SQLALCHEMY_DATABASE_URI = environ['MYSQL_URL']
SQLALCHEMY_TRACK_MODIFICATIONS = True
```

### Flaskアプリの定義
```python3:app.py
from flask import Flask

import settings
from api import api

# (今回使っていない)
from model import db

# Flask本体
app = Flask(__name__)


def configure_app(flask_app=Flask):
    # DB接続先情報やSwaggerの表示形式を指定
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE


def initialize_app(flask_app=Flask):
    # FlaskへAPIやDB情報を登録
    configure_app(flask_app)
    api.init_app(flask_app)
    db.init_app(flask_app)
    db.create_all(app=flask_app)


def main():
    # Flaskを初期化して実行
    initialize_app(app)
    app.run(host='0.0.0.0', debug=settings.DEBUG)


if __name__ == '__main__':
    main()
```

## 具体のAPI作成
### APIのNamespaceとJSONモデルの定義
```python3:api/router.py
import sys
sys.path.append('../')

import json
from flask_restplus import Namespace, fields, Resource

import sharelib.dbconnect as dblib


# Namespace初期化
router_namespace = Namespace('router', description='Routerのエンドポイント')

# JSONモデルの定義
router = router_namespace.model('Router',{
  'host': fields.String(
    require=True,
    description='適当に',
    example='example'
  ),
  .
  .
  .
  .
  # 返って欲しいJSONモデルを作成
})

# 実際のエンドポイントの追加
@router_namespace.route('/<string:host>')
class router(Resource):
  @router_namespace.marshal_list_with(router)
  def get(self):
    patterns = ['bldgname', 'chassis_serial', 'commercial_rack']
    output = {"host":host}
    
    for pattern in patterns:
      output.update(info(host, patterm))
    return output


# host名とpatternでrouterDBから情報を引っ張る
def info(host, pattern):
  DB = dblib.DBconnectforPhoenix()
  sql, header = DB.create_sql(pattern, host)
  str = DB.get_routerdb_data(sql, header).decode()
  return json.loads(str)
```


### APIエンドポイントの登録
```python3:api/__init__.py
from flask_restplus import Api

# 【追記】APIフォルダからAPIのnamespaceをインプット
from api.router import router_namespace


# API情報を指定して初期化
api = Api(
    title='Test API',
    version='1.0',
    description='Swaggerを統合したREST APIのサンプル'
)

# 【追記】エンドポイントの登録
api.add_namespace(router_namespace)
```

## 注意点
* docker-compose.ymlフィアルのbuild/argsにproxyを定義
* flaskサーバの実行はbashの実行より早いため、.bash_profileに書かれた環境変数が機能しません。docker-compose.ymlファイルのenvironmentに記入

